<?php

namespace App\Events;

use App\Entity\User;

class UserSubscribedEvent
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

}
<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EmailInputFormType;
use App\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailSubscriptionController extends AbstractController
{

    private UserService $emailService;
    public function __construct(UserService $emailService)
    {
        $this->emailService = $emailService;
    }

    #[Route('/email/subscription', name: 'app_email_subscription')]
    public function create(Request $request)
    {
        $form = $this->createForm(EmailInputFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $data = $form->getData();
            $this->emailService->getUserData($data);
        }
        return $this->render('email_subscription/index.html.twig', [
            'form' => $form,
        ]);
    }
}

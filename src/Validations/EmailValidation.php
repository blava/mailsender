<?php

namespace App\Validations;

use Symfony\Component\Config\Definition\Builder\ValidationBuilder;

class EmailValidation extends ValidationBuilder
{
    public function validate($data)
    {
        if (!empty($data->email) &&  filter_var($data->email , FILTER_VALIDATE_EMAIL))
            return true;
        return false;
    }
}
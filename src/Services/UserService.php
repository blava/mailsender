<?php

namespace App\Services;

use App\Entity\User;
use App\Events\UserSubscribedEvent;
use App\Validations\EmailValidation;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserService
{
    private EventDispatcherInterface $eventDispatcher;
//    private EmailValidation $validation;
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
       $this->eventDispatcher = $eventDispatcher;
//       $this->validation = $validation;
    }
    public function getUserData(User $user ): void
    {
        $this->eventDispatcher->dispatch(new UserSubscribedEvent($user));
    }

}
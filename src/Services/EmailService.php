<?php

namespace App\Services;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class EmailService
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmail($parameters)
    {
        $email = (new TemplatedEmail())
            ->from('sales@blava.ge')
            ->to(new Address($parameters['email'], $parameters['name']))
            ->subject('Order Details')
            ->htmlTemplate('emails/order-confirmation.html.twig')
            ->context([
                        'delivery_date' => date_create('+3 days'),
                        'order_number' => rand(100, 50000)
            ]);
            $this->mailer->send($email);
        return new Response("Email Sent", 200);
    }

}
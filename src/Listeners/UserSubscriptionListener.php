<?php

namespace App\Listeners;

use App\Events\UserSubscribedEvent;
use App\Services\EmailService;
use App\Services\UserService;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserSubscriptionListener implements EventSubscriberInterface
{
    private EmailService $emailService;
    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public static function getSubscribedEvents():array
    {
        return [
            UserSubscribedEvent::class=> 'onReceivingEvent'
        ];
    }

    public function onReceivingEvent(UserSubscribedEvent $event):void
    {
        $user = $event->getUser();
        $this->emailService->sendEmail(['email' => $user->getEmail(), 'name' => $user->getName()]);
    }
}